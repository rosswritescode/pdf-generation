var pdfFiller   = require('pdffiller');

// https://www.pdfescape.com 
// https://www.pdflabs.com/tools/pdftk-server/  <--- Need to install

// TODO: Get from remote server or file system.
// TODO: Select correct PDF to pre-fill.
var sourcePDF = "./files/original/Demo_Editable_Pdf.pdf";

// TODO: Base on retrieved filename. Timestamp
var destinationPDF = "./files/filled/test_complete.pdf";


// TODO: Get from database/API call. OR get from passed parameters. POST?
// TODO: Authenticate user, make sure that we don't see data we're not supposed to.
var data = {
    "smalltext" : "John"
};


pdfFiller.fillForm( sourcePDF, destinationPDF, data, function(err) {
    if (err) {
        throw err;
    };

    console.log("PDF fill complete.");

    // TODO: Send to docusign - if this is not in "Preview" mode.
    // TODO: Upload completed file to storage bucket - if not in "Preview" mode
});